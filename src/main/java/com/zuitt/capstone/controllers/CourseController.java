package com.zuitt.capstone.controllers;

import com.zuitt.capstone.models.Course;
import com.zuitt.capstone.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class CourseController {

    @Autowired
    CourseService courseService;

    // Create course
    // Automatically deserialize JSON client/frontend input into a Java object via @RequestBody
    // ResponseEntity represents the whole HTTP response: status code, headers and body.
    @RequestMapping(value="/courses", method = RequestMethod.POST)
    public ResponseEntity<Object> createCourse(@RequestHeader(value="Authorization") String stringToken, @RequestBody Course course) {
        courseService.createCourse(course);
        return new ResponseEntity<>("Course created successfully", HttpStatus.CREATED);
    }

    // Get courses
    @RequestMapping(value="/courses", method = RequestMethod.GET)
    public ResponseEntity<Object> getCourses() {
        return new ResponseEntity<>(courseService.getCourses(), HttpStatus.OK);
    }

    @RequestMapping(value = "/courses/{courseid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteCourse(@PathVariable Long courseid){
        return courseService.deleteCourse(courseid);
    }

    @RequestMapping(value = "/courses/{courseid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateCourse(@PathVariable Long courseid, @RequestBody Course course){
        return courseService.updateCourse(courseid, course);
    }

}