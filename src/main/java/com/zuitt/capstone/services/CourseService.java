package com.zuitt.capstone.services;

import com.zuitt.capstone.models.Course;
import org.springframework.http.ResponseEntity;


public interface CourseService {
    void createCourse(Course course);
    Iterable<Course> getCourses();
    ResponseEntity deleteCourse(Long id);
    ResponseEntity updateCourse(Long id, Course course);

}